Demographics_Model - Contains R Code for a model that uses data from the US Census Bureau to predict the election.
Final_Model - Aggregates all the models. The values have been hardcoded for the sake of simplicity.
Twitter_Model - Contains R Code for a model that uses data from Twitter to predict the election.
Election_Twitter - An R Script to get data from Twitter into R.
Polling_Model - Contains R Code for a model that uses data from opinion polls to predict the election.
Weighted_Var - Contains code to calculate the weighted variance. Used by Polling_model.
opinion-lexicon-English - Contains positive and negative words. Used by Twitter_model.
demographics_data - Contains data used by Demographics_Model.