# candidate information in 2008
Obama=`2008.data`[which(`2008.data`$candidate=="Obama"),]
McCain=`2008.data`[which(`2008.data`$candidate=="McCain"),]

#state information in 2008 for Obama
##1. population
###rename each column
population1=population[-1,]
name=population[1,]
names(population1)=name
population=population1
###population:character-->numeric
population.numeric=population
for(i in 1:408){
    population.numeric[,i]=as.numeric(population[,i])
}
###select population stat
###total,female,20 to 24,median age,over 65,white,(veterans)
pop.stat=population.numeric[,c(4,14,39,84,104,159)]


##2. education
education1=education[-1,]
name=education[1,]
names(education1)=name
education=education1
###character-->numeric
education.numeric=education
for(i in 1:231){
    education.numeric[,i]=as.numeric(education[,i])
}
###select stat
###less than high school,combine 18-24 and over 25
lhs=rep(0,52)
for(i in 1:52){
    lhs[i]=(education.numeric[i,4]*education.numeric[i,10]+education.numeric[i,34]*(education.numeric[i,40]+education.numeric[i,46]))/(education.numeric[i,4]+education.numeric[i,34])
}
###graduate degree,25 and over
gd=education.numeric[,220]/education.numeric[,190]
###education stat
edu.stat=cbind(lhs,gd)
colnames(edu.stat)=c("percentage of less than high school","percentage of graduate level or professional degree")


##3. employment
employment1=employment[-1,]
name=employment[1,]
names(employment1)=name
employment=employment1
###character-->numeric
employment.numeric=employment
for(i in 1:551){
    employment.numeric[,i]=as.numeric(employment[,i])
}
###select stat
#unemployed,not in labor force,with private insurance
emp.stat=employment.numeric[,c(22,30,390)]


##4.income
income1=income[-1,]
name=income[1,]
names(income1)=name
income=income1
###character-->numeric
income.numeric=income
for(i in 1:131){
    income.numeric[,i]=as.numeric(income[,i])
}
###select stat
#median income
inc.stat=income.numeric[,92]


##5.industry
industry1=industry[-1,]
name=industry[1,]
names(industry1)=name
industry=industry1
###character-->numeric
industry.numeric=industry
for(i in 1:279){
    industry.numeric[,i]=as.numeric(industry[,i])
}
###select stat
#none


##6.poverty
poverty1=poverty[-1,]
name=poverty[1,]
names(poverty1)=name
poverty=poverty1
###character-->numeric
poverty.numeric=poverty
for(i in 1:279){
    poverty.numeric[,i]=as.numeric(poverty[,i])
}
###select stat
#poverty for African American
pov.stat=poverty.numeric[,62]


##combine all stat
Obama.stat = cbind(Obama[,c(2,3,6:10)],pop.stat[-52,],edu.stat[-52,],emp.stat[-52,],inc.stat[1:51],pov.stat[-52])
names(Obama.stat)[19]="median income"
names(Obama.stat)[20]="poverty for African American"

##linear regression model for Obama in 2008
Obama.stat.numeric = Obama.stat[,c(-2,-3,-4,-5,-6,-7)]
fit0 = lm(percentage~1,data=Obama.stat.numeric)
fit1 = lm(percentage~.,data=Obama.stat.numeric)
library(MASS)
foo=stepAIC(fit0,direction="both",scope=list(upper=fit1,lower=fit0))
formula(foo)
fit.linear = lm(formula(foo),data=Obama.stat.numeric)###linear model###
summary(fit.linear)
##predict
percentage.predict.2008=predict(fit.linear)
Obama.stat=cbind(Obama.stat,percentage.predict.2008)
##error percentage
error.percentage=rep(0,51)
for(i in 1:51){
    error.percentage[i]=(Obama.stat[i,21]-Obama.stat[i,1])/Obama.stat[i,1]
}
Obama.stat=cbind(Obama.stat,error.percentage)###final statistics table"Obama.stat"###

####linear regression model for McCain in 2008
McCain.stat = cbind(McCain[,c(2,3,6:10)],pop.stat[-52,],edu.stat[-52,],emp.stat[-52,],inc.stat[1:51],pov.stat[-52])
names(McCain.stat)[19]="median income"
names(McCain.stat)[20]="poverty for African American"
McCain.stat.numeric = McCain.stat[,c(-2,-3,-4,-5,-6,-7)]
fit2 = lm(percentage~1,data=McCain.stat.numeric)
fit3 = lm(percentage~.,data=McCain.stat.numeric)
foo1=stepAIC(fit2,direction="both",scope=list(upper=fit3,lower=fit2))
formula(foo1)
fit.linear.McCain = lm(formula(foo1),data=McCain.stat.numeric)###linear model###
summary(fit.linear.McCain)
##predict
percentage.predict.2008=predict(fit.linear.McCain)
McCain.stat=cbind(McCain.stat,percentage.predict.2008)
##error percentage
error.percentage=rep(0,51)
for(i in 1:51){
    error.percentage[i]=(McCain.stat[i,21]-McCain.stat[i,1])/McCain.stat[i,1]
}
McCain.stat=cbind(McCain.stat,error.percentage)###final statistics table"McCain.stat"###

#================================================================================#
#predict 2016 for Clinton in Florida
##1.2016 population
population.2016=`2016population`
pop1=population.2016[-1,]
name=population.2016[1,]
names(pop1)=name
population.2016=pop1
##Florida population
F.population=subset(population.2016,Geography=="Florida")
##population stat
###total,female,20 to 24,median age,over 65,white,(veterans)
F.pop.stat=F.population[,c(4,22,67,148,184,283)]

##2.2016 education
education.2016=`2016education`
edu1=education.2016[-1,]
name=education.2016[1,]
names(edu1)=name
education.2016=edu1
##Florida education
F.education=subset(education.2016,Geography=="Florida")
##education stat
###less than high school, graduate degree or professional degree
gd=as.numeric(F.education[,220])/(as.numeric(F.education[,220]))
F.edu.stat=cbind(F.education[,10],gd)

##3.2016 employment
employment.2016=`2016employment`
emp1=employment.2016[-1,]
name=employment.2016[1,]
names(emp1)=name
employment.2016=emp1
##Florida employment
F.employment=subset(employment.2016,Geography=="Florida")
##employment stat
##unemployed,not in labor force,with private insurance
F.emp.stat=F.employment[,c(22,30,390)]

##4.2016 income
income.2016=`2016income`
inc1=income.2016[-1,]
name=income.2016[1,]
names(inc1)=name
income.2016=inc1
##Florida income
F.income=subset(income.2016,Geography=="Florida")
##income stat
##median income
F.inc.stat=F.income[92]

##5.2016 industry
industry.2016=`2016industry`
ind1=industry.2016[-1,]
name=industry.2016[1,]
names(ind1)=name
industry.2016=ind1
##Florida industry
F.industry=subset(industry.2016,Geography=="Florida")
##no industry stat (just in case you think about some factors in the region of industry)

##6.2016 poverty
poverty.2016=`2016poverty`
pov1=poverty.2016[-1,]
name=poverty.2016[1,]
names(pov1)=name
poverty.2016=pov1
##Florida income
F.poverty=subset(poverty.2016,Geography=="Florida")
##poverty stat
#poverty for African American
F.pov.stat=F.poverty[86]

##2016 Florida for Clinton
Clinton=`2016.data`[which(`2016.data`$candidate=="Clinton"&&`2016.data`$state=="Florida"),]
Florida.stat=cbind(F.pop.stat,F.edu.stat,F.emp.stat,F.inc.stat,F.pov.stat)
Florida.stat=as.numeric(Florida.stat)
Florida.stat[c(7)]=17.0 ##I don't know why number change from original data.
Florida.stat=t(Florida.stat)
name=names(Obama.stat)[8:20]
names(Florida.stat)=name
Florida.stat=as.data.frame(Florida.stat)
Florida.stat.Clinton=cbind(Clinton[,-2],Florida.stat)
##predict
percentage.Clinton=predict(fit.linear,Florida.stat)
Florida.stat.Clinton=cbind(Florida.stat.Clinton,percentage.Clinton)##52.16667%

##2016 Florida for Trump
Trump=`2016.data`[which(`2016.data`$candidate=="Trump"&`2016.data`$state=="Florida"),]
Florida.stat.Trump=cbind(Trump[,-2],Florida.stat)
##predict
percentage.Trump=predict(fit.linear.McCain,Florida.stat)
Florida.stat.Trump=cbind(Florida.stat.Trump,percentage.Trump)##48.866%  
#sum of Clinton and Trump is over 100 (101.0327)!!!

##plot
Clinton.percentage = 52.16667/(52.16667+48.866)
Trump.percentage = 48.866/(52.16667+48.866)
predict.2016 = data.frame(c("Clinton","Trump"),c(Clinton.percentage,Trump.percentage))
library("ggplot2")
library("gcookbook")
names(predict.2016)=c("candidate","percentage")
ggplot(predict.2016,aes(x=candidate,y=percentage))+geom_area()

##=========================================================================##
##diagnostics
res=fit.linear$residuals
qqnorm(res)
qqline(res,col="red")
#residuals
par(mfrow=c(2,3))
plot(Obama.stat.numeric$`median income`,res);abline(0,0)
plot(Obama.stat.numeric$`2008  Estimate; SEX AND AGE - Total population - Female`,res);abline(0,0)
plot(Obama.stat.numeric$`2008  Estimate; RACE - One race - White`,res);abline(0,0)
plot(Obama.stat.numeric$`2008  Estimate; SEX AND AGE - Total population - Median age (years)`,res);abline(0,0)
plot(Obama.stat.numeric$`Percent; EMPLOYMENT STATUS - In labor force - Civilian labor force - Unemployed`,res);abline(0,0)
plot(Obama.stat.numeric$`2008  Estimate; SEX AND AGE - Total population - 20 to 24 years`,res);abline(0,0)
#residuals vs. fit model
par(mfrow=c(1,1))
plot(predict(fit.linear),res);abline(0,0)
#influential points
#leverage
lev=hat(model.matrix(fit.linear))
2*sum(lev)/(51*6)
plot(lev);abline(0.27,0)
#cook
cook=cooks.distance(fit.linear)
plot(cook)
#residuals vs leverage
plot(lev,res)
